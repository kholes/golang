package main

import (
	"fmt"
	"gitlab.com/kholes/golang/greeting"
)

func main() {
	message := greeting.Hello("Adam")
	fmt.Println(message)
}
